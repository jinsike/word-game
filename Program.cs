using System;
using System.Collections.Generic;
using System.Linq;
using System.Resources;
using System.Windows.Forms;

namespace WordGame
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());
        }
    }

    class MainForm : Form
    {
        private List<string> words = new List<string>()
        {
            "автобус",
            "автомобиль",
            "трактор",
            "танк",
            "велосипед"
        };

        private Random random = new Random();
        private string currentWord;
        private int attemptsLeft = 5;
        private int attemptsAmount = 0;
        private List<char> guessedChars = new List<char>();

        private Label titleLabel;
        private Label wordLabel;
        private Label resultLabel;
        private TextBox inputBox;
        private PictureBox pictureBox1;

        public MainForm()
        {
            Text = "Игра в слова";
            Width = 675;
            Height = 500;
            FormBorderStyle = FormBorderStyle.FixedSingle;
            MaximizeBox = false;

            titleLabel = new Label();
            titleLabel.Text = "Отгадайте слово";
            titleLabel.Font = new System.Drawing.Font("Arial", 16, System.Drawing.FontStyle.Bold);
            titleLabel.AutoSize = true;
            titleLabel.Location = new System.Drawing.Point(10, 10);
            Controls.Add(titleLabel);

            wordLabel = new Label();
            wordLabel.Font = new System.Drawing.Font("Arial", 16, System.Drawing.FontStyle.Bold);
            wordLabel.AutoSize = true;
            wordLabel.Location = new System.Drawing.Point(10, 50);
            Controls.Add(wordLabel);

            resultLabel = new Label();
            resultLabel.Font = new System.Drawing.Font("Arial", 12);
            resultLabel.AutoSize = true;
            resultLabel.Location = new System.Drawing.Point(10, 100);
            Controls.Add(resultLabel);

            inputBox = new TextBox();
            inputBox.Font = new System.Drawing.Font("Arial", 16);
            inputBox.Size = new System.Drawing.Size(50, 30);
            inputBox.Location = new System.Drawing.Point(10, 150);
            inputBox.MaxLength = 1;
            inputBox.TextChanged += InputBox_TextChanged;
            Controls.Add(inputBox);

            pictureBox1 = new PictureBox();


            // 
            // pictureBox1
            // 
            pictureBox1.Dock = DockStyle.Fill;
            pictureBox1.Image = Image.FromFile("C:\\Users\\jinsi\\source\\repos\\WinFormsApp1\\pole.gif");

            pictureBox1.Location = new Point(100, 100);
            pictureBox1.Name = "pictureBox1";
            pictureBox1.Size = new Size(20, 20);
            //pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            pictureBox1.TabIndex = 2;
            pictureBox1.TabStop = false;
            pictureBox1.BorderStyle = BorderStyle.FixedSingle;
            pictureBox1.BackColor = System.Drawing.Color.FromArgb(123);
            Controls.Add(pictureBox1);


            StartNewGame();
        }

        private void StartNewGame()
        {
            guessedChars.Clear();
            currentWord = words[random.Next(words.Count)];

            string maskedWord = new string('-', currentWord.Length);
            wordLabel.Text = maskedWord;

            char[] maskedWord2 = wordLabel.Text.ToCharArray();
            maskedWord2[0] = currentWord.First();
            maskedWord2[^1] = currentWord.Last();
            Console.WriteLine(maskedWord2.Last());
            wordLabel.Text = string.Join("", maskedWord2);
            resultLabel.Text = "";

            attemptsLeft = 5;
            attemptsAmount = 0;
        }

        private void InputBox_TextChanged(object sender, EventArgs e)
        {
            char guessChar = inputBox.Text.ToLower().FirstOrDefault();
            if (guessChar == default)
            {
                return;
            }

            inputBox.Text = "";
            if (guessedChars.Contains(guessChar))
            {
                resultLabel.Text = "Эта буква уже была отгадана";
                return;
            }

            guessedChars.Add(guessChar);

            if (currentWord.Contains(guessChar))
            {
                attemptsAmount++;
                char[] maskedWord = wordLabel.Text.ToCharArray();
                for (int i = 0; i < currentWord.Length; i++)
                {
                    if (currentWord[i] == guessChar)
                    {
                        maskedWord[i] = guessChar;
                    }
                }
                wordLabel.Text = new string(maskedWord);
            }
            else
            {
                attemptsLeft--;
                attemptsAmount++;
                resultLabel.Text = $"Этой буквы нет в слове, осталось {attemptsLeft} попыток";
            }

            if (attemptsLeft == 0)
            {
                resultLabel.Text = $"Вы проиграли! Загаданное слово: {currentWord}, интелект: {attemptsAmount - (currentWord.Length - 2) / (currentWord.Length - 2)}";
                inputBox.TextChanged -= this.InputBox_TextChanged;
                inputBox.Enabled = false;
                //StartNewGame();
            }

            if (!wordLabel.Text.Contains("-"))
            {
                resultLabel.Text = $"Поздравляем, вы выиграли!, интелект: {attemptsAmount - (currentWord.Length - 2) / (currentWord.Length - 2)}";
                resultLabel.BackColor = Color.Green;
                resultLabel.ForeColor = Color.White;
                inputBox.TextChanged -= this.InputBox_TextChanged;
                inputBox.Enabled = false;
                //StartNewGame();
            }
        }


    }
}
